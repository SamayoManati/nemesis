#! /usr/bin/env python3

# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import sys
import os
import typing
from nemesis.constants import *
try:
	import pygame
except ModuleNotFoundError:
	print("The pygame module is required to run this program.")
	sys.exit(ERR_NO_PYGAME_MODULE)
from nemesis.Listener import EventsListener
from nemesis.Renderer import *
from nemesis.Updater import *
from nemesis.Player import *
from nemesis.GameScene import *
from nemesis.MenuScene import *

class Application(EventsListener, Updater):
	"""Représente le jeu."""
	def __init__(self):
		pygame.init()

		os.environ['SDL_VIDEO_CENTERED'] = '1'

		self._running = True
		#self._display_surface = pygame.display.set_mode((0,0))
		self._display_surface = pygame.display.set_mode((1080, 512))

		self._play_time = 0

		self._game_scenes = {}
		self._menu_scenes = {}
		self._current_game_scene = ""
		self._current_menu_scenes = []

		internal_set_application(self)
	
	def register_game_scene(self, scene: GameScene):
		"""Enregistre une scène de jeu dans la liste des scène du jeu.

		Les scènes du jeu sont identifiées par leur nom. Par conséquent,
		il ne peut pas y avoir plusieurs scènes avec le même nom. En
		revanche, il peut y avoir une `GameScene` et une `MenuScene` avec le
		même nom.

		Si on essaye d'enregistrer une scène avec un nom déjà utilisé,
		la méthode lèvera une `NameError`.
		
		scene : `GameScene`"""
		if not scene.get_name() in self._game_scenes:
			self._game_scenes[scene.get_name()] = scene
		else:
			raise NameError("Une scène nommée \"" + scene.get_name() + "\" existe déjà...")
		return
	
	def register_menu_scene(self, scene: MenuScene):
		"""Enregistre un menu dans la liste des scène du jeu.

		Les scènes du jeu sont identifiées par leur nom. Par conséquent,
		il ne peut pas y avoir plusieurs scènes avec le même nom. En
		revanche, il peut y avoir une `GameScene` et une `MenuScene` avec le
		même nom.

		Si on essaye d'enregistrer une scène avec un nom déjà utilisé,
		la méthode lèvera une `NameError`.
		
		scene : `MenuScene`"""
		if not scene.get_name() in self._menu_scenes:
			self._menu_scenes[scene.get_name()] = scene
		else:
			raise NameError("Une scène nommée \"" + scene.get_name() + "\" existe déjà...")
		return
	
	def set_current_game_scene(self, scene_name: str):
		"""Définit quelle est la scène de jeu actuelle, en se basant sur son
		nom.
		
		Si la scène n'existe pas, lève une `NameError`."""
		if scene_name in self._game_scenes:
			self._current_game_scene = scene_name
		else:
			raise NameError("Il n'y a pas de scène nommée \"" + scene_name + "\"...")
		return
	
	def get_current_game_scene(self) -> str:
		"""Retourne le nom de la scène de jeu actuelle."""
		return self._current_game_scene
	
	def get_game_scenes(self) -> typing.Dict[str, GameScene]:
		"""Retourne la liste des `GameScene`s enregistrées."""
		return self._game_scenes
	
	def get_menu_scenes(self) -> typing.Dict[str, MenuScene]:
		"""Retourne la liste des `MenuScene`s enregistrées."""
		return self._menu_scenes

	def set_title(self, title: str):
		"""Modifie le titre affiché du jeu."""
		pygame.display.set_caption(title)
		return
	
	def get_title(self) -> str:
		return pygame.display.get_caption()
	
	def get_display_surface(self) -> pygame.Surface:
		"""Retourne une référence vers la surface de dessin racine du jeu."""
		return self._display_surface

	def exit(self) -> None:
		"""Ferme le jeu."""
		self._running = False
	
	def process_event(self, event):
		if event.type == pygame.QUIT:
			self._running = False
		self._game_scenes[self._current_game_scene].process_event(event)
		return
	
	def update(self):
		self._game_scenes[self._current_game_scene].update()
		return

	def render(self):
		self._display_surface.fill((0,0,0))

		"""sysfont = pygame.font.SysFont("Ubuntu", 12)
		wiptext = sysfont.render("Bientôt, ici, un jeu", True, (255, 255, 255), None)
		self._display_surface.blit(wiptext, (self._display_surface.get_width() / 2, self._display_surface.get_height() / 2))"""

		self._game_scenes[self._current_game_scene].render(self._display_surface)

		pygame.display.flip()
		return

	def cleanup(self):
		pygame.quit()
		return

	def execute(self):
		while self._running:
			internal_set_delta_time(T_CLOCK.get_time() / 1000)
			for event in pygame.event.get():
				self.process_event(event)
			self.update()
			self.render()
			__ms = T_CLOCK.tick(T_FPS)
			self._play_time += __ms / 1000
		self.cleanup()
		sys.exit(0)
		return
