# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame

class Renderer:
	"""Cette classe est une classe abstraite dont héritent les composants qui
	peuvent afficher à l'écran."""

	def render(self, surface: pygame.Surface):
		"""Effectue le rendu à l'écran."""
		raise NotImplementedError