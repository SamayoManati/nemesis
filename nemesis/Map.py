# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame
import os.path
import json
import typing

from nemesis.Renderer import *
from nemesis.Sprite import *
from nemesis.Tileset import *
from nemesis.constants import *

class MapTilesLayer:
	"""Contient les informations d'un calque de map de tiles."""
	def __init__(self, datas: list, id: int, name: str, opacity: float, visible: bool):
		self.__datas = datas
		self.__id = id
		self.__name = name
		self.__opacity = opacity
		self.__visible = visible
	
	def get_datas(self) -> list:
		"""Retourne les données du calque."""
		return self.__datas
	
	def get_id(self) -> int:
		"""Retourne l'ID global du calque."""
		return self.__id
	
	def get_name(self) -> str:
		"""Retourne le nom du calque."""
		return self.__name
	
	def get_opacity(self) -> float:
		"""Retourne l'indice d'opacité du calque (entre 0 et 1)"""
		return self.__opacity
	
	def get_visible(self) -> bool:
		"""Retourne si le calque est visible."""
		return self.__visible

class MapObjectsLayer:
	"""Contient les informations d'un calque d'objets d'une map"""
	def __init__(self, objects: list, id: int, name: str, opacity: float, visible: bool):
		self.__objects = objects
		self.__id = id
		self.__name = name
		self.__opacity = opacity
		self.__visible = visible
	
	def get_id(self) -> int:
		"""Retourne l'ID global du calque."""
		return self.__id

	def get_name(self) -> str:
		"""Retourne le nom du calque."""
		return self.__name
	
	def get_opacity(self) -> float:
		"""Retourne l'opacité du calque (entre 0 et 1)"""
		return self.__opacity
	
	def get_visible(self) -> bool:
		"""Retourne si le calque est visible."""
		return self.__visible
	
	def get_objects(self) -> list:
		"""Retourne les objets du calque d'objets."""
		return self.__objects

class MapObject:
	"""La classe `MapObject` représente un objet placé sur un calque d'objets
	sur la carte. C'est la classe-mère pour tous les types d'objets placés
	sur la carte (points de spawn, zones de spawn...)"""
	def __init__(self, position: pygame.math.Vector2, name: str, type: str):
		self.__position = position
		self.__name = name
		self.__type = type

	def get_position(self) -> pygame.math.Vector2:
		"""Retourne la position de l'objet."""
		return self.__position
	
	def get_name(self) -> str:
		"""Retourne le nom de l'objet."""
		return self.__name

	def get_type(self) -> str:
		"""Retourne le type de l'objet."""
		return self.__type

class MapSpawnPoint(MapObject):
	"""Représente un point de spawn pour une entité."""
	pass

class Map(Renderer):
	"""La classe Map représente une map de jeu. C'est la couche qui sera tracée
	pour afficher la map. Elle ne possède ni de méthode loop ni de méthode
	process_event, mais seulement d'une méthode render.
	
	Si le fichier de map n'existe pas, lève une `FileNotFoundError`"""
	def __init__(self, name: str):
		self.__name = name
		self.__initialized = False

		joined_name = os.path.join(F_DIR_MAPS, name + ".json")
		
		if not os.path.exists(joined_name):
			raise FileNotFoundError("Cannot find map file \"" + joined_name + "\"...")

		with open(joined_name, "r") as fp:
			decoded = json.loads(fp.read())
		
		self.__size = pygame.math.Vector2(decoded["width"], decoded["height"])
		self.__orientation = decoded["orientation"]
		self.__render_order = decoded["renderorder"]
		self.__tilesets = []
		for current_tileset in decoded["tilesets"]:
			ts_name = current_tileset["source"]
			ts_name = os.path.split(ts_name)[1] # on récupère le dernier composant du chemin vers le tileset, car c'est le nom du tileset
			ts = Tileset(ts_name)
			gid = current_tileset["firstgid"]
			self.__tilesets.append((ts, gid))
		self.__layers = []
		for current_layer in decoded["layers"]:
			if current_layer["type"] == "tilelayer":
				# un calque de tiles
				lay = MapTilesLayer(current_layer["data"], current_layer["id"], current_layer["name"], current_layer["opacity"], current_layer["visible"])
				self.__layers.append(lay)
			elif current_layer["type"] == "objectgroup":
				# si c'est un calque d'objets
				layer_name = current_layer["name"]
				objects = []
				if layer_name == "Named Entities":
					# le calque des points de spawn des entités
					for current_object in current_layer["objects"]:
						x = current_object["x"]
						y = current_object["y"]
						if current_object["type"] == "":
							raise AttributeError("Untyped entity spawn point on Named Entities layer (id " + str(current_object["id"]) + ")...")
						obj = MapSpawnPoint(pygame.math.Vector2(x, y), current_object["name"], current_object["type"])
						objects.append(obj)
				lay = MapObjectsLayer(objects, current_layer["id"], current_layer["name"], current_layer["opacity"], current_layer["visible"])
				self.__layers.append(lay)

	def init(self):
		"""Initialise les données de map au premier rendu.
		
		Comme par exemple la position des entités."""
		if self.__initialized:
			return
		self.__initialized = True
		# initialisation des entités de la map (position...)
		for current_layer in self.__layers:
			if type(current_layer) is not MapObjectsLayer:
				continue
			if current_layer.get_name() == "Named Entities":
				# claque des entités nommées
				game_scene = APPLICATION.HANDLER.get_game_scenes()[APPLICATION.HANDLER.get_current_game_scene()]
				for current_object in current_layer.get_objects():
					entity = game_scene.get_named_entity(current_object.get_name(), current_object.get_type())
					entity._position = pygame.math.Vector2(current_object.get_position())

	def render(self, surface: pygame.Surface):
		self.init()

		game_scene = APPLICATION.HANDLER.get_game_scenes()[APPLICATION.HANDLER.get_current_game_scene()]

		for current_layer in self.__layers:
			# pour chaque calque, du premier au dernier
			if type(current_layer) is MapTilesLayer:
				# un calque de tiles
				position = pygame.math.Vector2(0, 0)
				for current_tile in current_layer.get_datas():
					tile_tileset = self.get_tileset_by_id(current_tile)
					if tile_tileset is not None:
						tile: Tile = tile_tileset[0].get_tile(current_tile - tile_tileset[1])
						tile_sprite = tile.sprite.get_variant(tile.variant)
						if G_USE_SPRITES_ZOOM:
							pos = (position.x * G_CELL_WIDTH * G_SPRITES_ZOOM, position.y * G_CELL_HEIGHT * G_SPRITES_ZOOM)
						else:
							pos = (position.x * G_CELL_WIDTH, position.y * G_CELL_HEIGHT)
						surface.blit(tile_sprite, pos)
					if position.x + 1 < self.get_size().x:
						position.x += 1
					else:
						position.x = 0
						position.y += 1
			elif type(current_layer) is MapObjectsLayer:
				# un calque d'objets
				if current_layer.get_name() == "Named Entities":
					# un calque d'entités nommées
					for current_object in current_layer.get_objects():
						entity = game_scene.get_named_entity(current_object.get_name(), current_object.get_type())
						entity.render(surface)

	def get_name(self) -> str:
		"""Retourne le nom de la map. Le nom étant en lecture-seule, il est
		impossible de le modifier."""
		return self.__name
	
	def get_size(self) -> pygame.math.Vector2:
		"""Retourne la taille de la map."""
		return self.__size

	def get_orientation(self) -> str:
		"""Retourne l'orientation de la map."""
		return self.__orientation
	
	def get_render_order(self) -> str:
		"""Retourne l'ordre de rendu de la map."""
		return self.__render_order
	
	def get_tilesets(self) -> list:
		"""Retourne la liste des tilesets de la map, sous la forme
		`list[tuple(Tileset, int)]`, avec `Tileset` le tileset et `int` l'ID
		global de la première tile du tileset."""
		return self.__tilesets
	
	def get_tileset(self, name: str) -> tuple:
		"""Retourne un tileset de la map et son ID global de première tile,
		selon son nom.
		
		Retour sous la forme `tuple(Tileset, int)`. Si le tileset nommé
		n'existe pas, lève une `NameError`"""
		for current_tuple in self.__tilesets:
			if current_tuple[0].get_name() == name:
				return current_tuple
		raise NameError("Cannot find a tileset named \"" + name + "\"...")

	def get_tileset_by_id(self, id: int) -> tuple:
		"""Retourne le tileset qui contient la tile selon son ID.
		
		Si l'ID de la tile est plus grand ou égal à l'ID global du tileset, et
		plus petit ou égal au nombre de tiles que ce tileset contient (pour
		éviter de retourner le dernier tileset de la liste si jamais on
		demande le tileset d'une tile trop haute (par exemple la tile 854 alors
		que le dernier tileset de la liste ne contient que 200 tiles.)), alors
		retourne un tuple contenant une référence vers ce tileset, et l'ID
		global de ce tileset.
		
		Si aucun tileset correspondant à la tile n'est trouvé, retourne `None`."""
		i = len(self.__tilesets) - 1
		while i >= 0:
			if id >= self.__tilesets[i][1] and id <= self.__tilesets[i][0].get_tiles_count():
				return self.__tilesets[i]
			i -= 1

	def has_layer(self, layer_name: str) -> bool:
		"""Retourne si la carte contient un calque nommé `layer_name`"""
		for current_layer in self.__layers:
			if current_layer.get_name() == layer_name:
				return True
		return False
	
	def get_layer_by_name(self, name: str) -> typing.Union[MapTilesLayer, MapObjectsLayer]:
		"""Retourne un calque selon son nom.

		Si il n'y a aucun calque avec ce nom, lève une `NameError`."""
		if not self.has_layer(name):
			raise NameError("Cannot find layer \"" + name + "\"...")
		for current_layer in self.__layers:
			return current_layer
	
	def get_tile_at(self, location: pygame.math.Vector2, layer_name: str) -> Tile:
		"""Retourne la Tile à un point de la map.

		location : un `Vector2` contenant la case de la map.

		layer_name : le nom du calque sur lequel chercher la tile.
		
		Si le point spécifié est hors de la map, lève une `IndexError`. Si le
		calque demandé n'existe pas, ou que ce n'est pas un calque de tiles,
		lève une `NameError`. Si la fonction ne parvient pas à trouver le
		tileset de la tile, retourne `None`."""
		if location.x > self.__size.x or location.y > self.__size.y:
			raise IndexError("Reference to a outmap point : X:" + str(location.x) + " Y:" + str(location.y) + "...")
		if not self.has_layer(layer_name) or type(self.get_layer_by_name(layer_name)) is not MapTilesLayer:
			raise NameError("Cannot find tiles layer \"" + layer_name + "\"...")
		datas = self.get_layer_by_name(layer_name).get_datas()
		index = 0
		index += location.y * self.get_size().x
		index += location.x
		index = int(index)
		tile_id = datas[index]
		tile_tileset = self.get_tileset_by_id(tile_id)
		if tile_tileset is None:
			return None
		return tile_tileset[0].get_tile(tile_id - tile_tileset[1])
	
	def get_tiles_at(self, location: pygame.math.Vector2) -> typing.List[Tile]:
		"""Retourne la liste des `Tile`s à un point donné de la map.

		Si le point spécifié est hors de la map, lève une `IndexError`. Si la
		fonction ne parvient pas à trouver le tileset de la tile, retourne
		`None`."""
		layers_names = [x.get_name() for x in self.__layers]
		return [self.get_tile_at(location, x) for x in layers_names]

def pixels2cell(position: pygame.math.Vector2) -> pygame.math.Vector2:
	"""Convertit une position en pixels vers une position en cases."""
	cell_pos = pygame.math.Vector2(position)
	cell_pos.x //= G_CELL_WIDTH
	cell_pos.y //= G_CELL_HEIGHT
	return cell_pos

def cell2pixels(position: pygame.math.Vector2) -> pygame.math.Vector2:
	"""Convertir une position en case vers une position en pixels."""
	pixels_pos = pygame.math.Vector2(position)
	pixels_pos.x *= G_CELL_WIDTH
	pixels_pos.y *= G_CELL_HEIGHT
	return pixels_pos