# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame
import os
import json
from nemesis.constants import *
from nemesis.Sprite import *

class Tile:
	"""Une struct qui représente une tile d'un tileset."""
	sprite: Sprite
	variant: str
	collisions_policy: bool

class Tileset:
	"""Représente un tileset."""
	def __init__(self, tileset_name: str):
		"""tileset_name : le nom du tileset à charger."""
		self.__name = tileset_name

		joined_name = os.path.join(F_DIR_TILESETS, tileset_name)
		if not joined_name.endswith(".json"):
			joined_name += ".json"

		if not os.path.exists(joined_name):
			raise FileNotFoundError("Cannot find the tileset file \"" + joined_name + "\"...")
		
		with open(joined_name, "r") as fp:
			decoded = json.loads(fp.read())
		
		self.__grid_height      = decoded["grid"]["height"]
		self.__grid_width       = decoded["grid"]["width"]
		self.__grid_orientation = decoded["grid"]["orientation"]
		self.__tileset_name     = decoded["name"]
		self.__tiles_count      = decoded["tilecount"]
		self.__tiles            = []

		for current_tile in decoded["tiles"]:
			tl_splited = os.path.split(current_tile["image"])
			tl_variant = os.path.splitext(tl_splited[1])[0]
			tl_sprite = os.path.split(tl_splited[0])[1]
			tl = Sprite(tl_sprite)
			#self.__tiles.append((tl, tl_variant))
			collisions_policy = False
			if "properties" in current_tile:
				for current_property in current_tile["properties"]:
					if current_property["name"] == "collisions_policy":
						collisions_policy = current_property["value"]
			tile = Tile()
			tile.sprite = tl
			tile.variant = tl_variant
			tile.collisions_policy = collisions_policy
			self.__tiles.append(tile)
	
	def get_name(self) -> str:
		"""Retourne le nom du tileset. Le nom est en lecture-seule et ne peut
		pas être changé."""
		return self.__name
	
	def get_grid_height(self) -> int:
		return self.__grid_height
	
	def get_grid_width(self) -> int:
		return self.__grid_width

	def get_grid_orientation(self) -> str:
		return self.__grid_orientation
	
	def get_tileset_name(self) -> str:
		"""Retourne le nom intérieur du tileset. A ne pas confondre avec `get_name`,
		qui retourne le nom donné au tileset dans le jeu. Celui-ci peut être différent.
		C'est le nom donné au tileset dans Tiled, et celui qui est utilisé pour
		y faire référence dans les maps qui l'utilisent."""
		return self.__tileset_name
	
	def get_tiles_count(self) -> int:
		"""Retourne le nombre de tiles que contient le tileset."""
		return self.__tiles_count
	
	def get_tiles(self) -> list:
		"""Retourne la liste des tiles du tileset, sous la forme
		`list[tuple(Sprite, str)]`, avec `str` la variante du sprite."""
		return self.__tiles
	
	def get_tile(self, local_id: int) -> Tile:
		"""Retourne une tile en fonction de son ID local.
		
		Si l'ID local n'est pas reconnu, lève une IndexError."""
		if len(self.__tiles) > local_id:
			return self.__tiles[local_id]
		raise IndexError("Cannot find local ID " + str(local_id) + "...")