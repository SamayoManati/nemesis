# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame
from nemesis.Scene import *
from nemesis.Map import *

class GameScene(Scene):
	"""Représente un niveau du jeu."""
	def __init__(self, name: str, game_map: Map = None):
		super().__init__(name)
		self.__map = game_map
		self.__entities = []
	
	def register_entity(self, entity):
		self.__entities.append(entity)
		return
	
	def get_map(self) -> Map:
		"""Retourne la map utilisée par la scène."""
		return self.__map
	
	def get_named_entity(self, name: str, type: str):
		"""Retourne une référence vers une entité de la scène en se référant à
		son nom et à sa classe. Si aucune entité correspondante n'a été
		trouvée, retourne `None`."""
		for current_entity in self.__entities:
			if current_entity.__class__.__name__ == type and current_entity.get_name() == name:
				return current_entity
		return
	
	def render(self, surface: pygame.Surface):
		if self.__map is not None:
			self.__map.render(surface)
		
		#for current_entity in self.__entities:
		#	current_entity.render(surface)
		return
	
	def update(self):
		for current_entity in self.__entities:
			current_entity.update()
		return
	
	def process_event(self, event):
		for current_entity in self.__entities:
			current_entity.process_event(event)
		return