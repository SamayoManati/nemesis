# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import os
from nemesis.constants import *
import pygame

class Sprite:
	"""Représente une collection de sprites, utilisables pour une entité ou une
	cellule d'une map. Chaque image d'un Sprite s'appelle une variante.
	
	Les différents sprites de la collection sont tous stockés au format PNG dans
	un même dossier, et ce dossier porte le nom du sprite. Les noms des images
	dedans (sans l'extension) servent de nom pour les variantes du sprite."""
	def __init__(self, spritename: str):
		"""spritename : le nom du dossier contenant toutes les variantes du sprite à charger."""
		self.__sprites = {}
		self.__sprite_name = spritename

		for root, dirs, files in os.walk(os.path.join(F_DIR_SPRITES, spritename)):
			for name in files:
				if name.endswith(".png"):
					used_name = name[:-4]
					self.__sprites[used_name] = pygame.image.load(os.path.join(root, name))
	
	def get_variant(self, name) -> pygame.Surface:
		"""Retourne la variante de sprite par son nom."""
		if name in self.__sprites:
			if G_USE_SPRITES_ZOOM:
				return pygame.transform.rotozoom(self.__sprites[name], 0, G_SPRITES_ZOOM)
			else:
				return self.__sprites[name]
		else:
			raise NameError("There are no sprite variant called \"" + name + "\" in that sprite")
	
	def has_variant(self, name) -> bool:
		"""Retourne si il y a une variante de sprite nommée `name` dans le sprite."""
		return name in self.__sprites
	
	def get_sprite_name(self) -> str:
		"""Retourne le nom du sprite chargé. Ce nom ne peut être changé."""
		return self.__sprite_name