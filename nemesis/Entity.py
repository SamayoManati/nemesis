# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame
import typing
from nemesis.Sprite import *
from nemesis.Renderer import *
from nemesis.Updater import *
from nemesis.Listener import *
from nemesis.constants import *
from nemesis.Map import pixels2cell

class Entity(Renderer, EventsListener, Updater):
	"""La classe-mère la plus basique pour la représentation des entités."""
	def __init__(self):
		self._sprite = None
		self._position = pygame.math.Vector2(0, 0)
		self._used_sprite = ""
		self.__destination = None
		self.__last_movement_time = 0
		self.__name = ""
	
	def load_sprite(self, spritename):
		"""Charge un sprite pour l'entité à partir de son nom. Si la valeur
		`None` est fournie, supprime le sprite utilisé."""
		if spritename is None:
			self._sprite = None
		else:
			self._sprite = Sprite(spritename)
		return
	
	def get_sprite(self) -> Sprite:
		"""Retourne le sprite de l'entité"""
		return self._sprite
	
	def set_position(self, position: pygame.math.Vector2):
		"""Définit la position de l'entité sur la carte, en pixels."""
		self._position = position
		return

	def get_position(self) -> pygame.math.Vector2:
		"""Retourne la position de l'entité sur la carte, en pixels.."""
		return self._position
	
	def set_used_sprite(self, variant_name: str):
		"""Définit la variante de sprite utilisée pour l'affichage du sprite
		de l'entité."""
		self._used_sprite = variant_name
		return
	
	def get_used_sprite(self) -> str:
		"""Retourne la variante de sprite utilisée pour l'affichage du
		sprite de l'entité."""
		return self._used_sprite
	
	def has_destination(self) -> bool:
		"""Retourne si l'entité a actuellement une destination ou non."""
		return self.__destination is not None
	
	def get_destination(self) -> typing.Union[pygame.math.Vector2, None]:
		"""Retourne la destination actuelle (en pixels) de l'entité. Si
		l'entité n'a pas de destination, retourne `None`."""
		return self.__destination
	
	def set_destination(self, destination: typing.Union[pygame.math.Vector2, None]):
		"""Définit la destination de l'entité (en pixels). Pour supprimer la
		destination, mettez `None` à `destination`."""
		self.__destination = destination
		return
	
	def set_name(self, name: str) -> None:
		"""Définit le nom de l'entité. Ce nom est utilisé par le système de map
		pour repérer quelle entité doit spawner où sur le calque des entités
		nommées."""
		self.__name = name
	
	def get_name(self) -> str:
		"""Retourne le nom de l'entité."""
		return self.__name
	
	def render(self, surface: pygame.Surface):
		if self._sprite is not None:
			# si il y a un sprite à afficher
			displayed_sprite = self._sprite.get_variant(self._used_sprite)
			surface.blit(displayed_sprite, self.get_position())
		return

	def update(self):
		# mouvement vers la destination
		if self.has_destination():
			if pygame.time.get_ticks() - self.__last_movement_time > 1000:
				# 1000 = 1 mouvement par seconde
				# si notre point de destination est plus haut que notre
				#  position, on va en haut
				if self.__destination.y < self._position.y:
					self._position.y -= G_CELL_HEIGHT
				elif self.__destination.y > self._position.y:
					self._position.y += G_CELL_HEIGHT
				elif self.__destination.x < self._position.x:
					self._position.x -= G_CELL_WIDTH
				elif self.__destination.x > self._position.x:
					self._position.x += G_CELL_WIDTH
				else:
					print("I have reach my destination")#TODO DEBUG
					self.set_destination(None)
				self.__last_movement_time = pygame.time.get_ticks()

	def process_event(self, event):
		pass