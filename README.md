# Nemesis

Nemesis est un moteur de jeu, codé en python et basé sur le moteur graphique pygame. Il est orienté sur la création de jeux 2D de type rpg / j-rpg.

Il est actuellement en version alpha.

## Pré-requis

 * python 3 (>= 3.6.9 conseillé)
 * pygame (>= 1.9.6 conseillé)

## Utilisation

Pour utiliser Nemesis, vous devez avoir installé le package pygame. Vous pouvez le faire en exécutant cette ligne de commande :

	$ pip3 install pygame --user

Ensuite, vous devez télécharger le dossier `nemesis` et le placer à la racine du dossier qui contiendra votre jeu.

Nemesis utilise une arborescence de fichiers particulière, qu'il vous faut respecter afin que le moteur fonctionne :

 * `/` : le dossier racine de votre jeu
 * `/resources` : le dossier qui contiendra tous vos assets
 * `/resources/sprites` : le dossier qui contiendra vos sprites, de personnages comme de tiles
 * `/resources/tilesets` : le dossier qui contiendra vos tilesets
 * `/resources/maps` : le dossier qui contiendra vos cartes de jeu

À part cela, vous être libre de placer les autres fichiers où bon vous semble. Pour plus d'informations, le PDF `manuel.pdf` dans le dossier `doc` contient la notice d'utilisation de Nemesis. Il est régulièrement mis à jour en même temps que le moteur.

Le fichier `test.py` et le dossier `resources` à la racine de ce dépôt sont les fichiers utilisés pour tester Nemesis. Vous pouvez les regarder si vous avez besoin d'aide sur comment construire la structure de votre jeu, mais ils ne sont absolument pas nécessaires au fonctionnement de Nemesis.

## Licence

Nemesis est sous licence GNU-GPL 3.