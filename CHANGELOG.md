# Changelog :

version alpha 2
================================================================================
 * Gestion du calque d'entités nommées sur les maps
 * C'est désormais la `Map` qui se charge d'appeler le rendu des entités, et non la `GameScene`

**Détails** :
 * Ajout de la méthode `Application.get_display_surface()` pour obtenir une référence vers la surface de dessin racine du jeu
 * Ajout de la méthode `Application.exit()` pour quitter le jeu
 * Ajout de la classe `MapObject`
 * Ajout de la classe `MapSpawnPoint`
 * Ajout de la méthode `Map.init()`

version alpha 1
================================================================================
 * Ajout de la struture du framework
 * Ajout de la possibilité d'afficher une carte
 * Gestion des calques de tiles pour les cartes
 * Ajout d'une option dans les constantes pour agrandir tous les sprites affichés : `G_USE_SPRITES_ZOOM` et `G_SPRITES_ZOOM`
 * Ajout de la fonction `pixels2cell` dans le module `Map` pour convertir une position en pixels vers une position en case
 * Ajout de la fonction `cell2pixels` dans le module `Map` pour convertir une position en case vers une position en pixels
 * Gestion des collisions de tiles
 * Ajout d'un path finding extrêmement basique pour les entités (non affecté par les collisions de tiles)

**Détails** :
 * Ajout de fonctions dans Map pour récupérer la tile à une case de la map
 * Ajout de la constante `APPLICATION.HANDLER` qui pointe sur l'instance d'`Application` du jeu
 * Ajout de la fonction `internal_set_application` pour définir sur quelle instance d'`Application` pointe la constante globale `APPLICATION.HANDLER`
 * Ajout de la fonction `Entity.has_destination`
 * Ajout de la fonction `Entity.get_destination`
 * Ajout de la fonction `Entity.set_destination`
 * Ajout de la propriété `Entity.__destination`
 * Ajout de la propriété `Entity.__last_movement_time`